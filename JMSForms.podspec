Pod::Spec.new do |s|
	s.name     = 'JMSForms'
	s.version  = '0.0.2'
	s.license  = 'MIT'
	s.summary  = 'Framework to simplify forms in iOS'
	s.homepage = 'https://buscarini@bitbucket.org/buscarini/jmsforms.git'
	s.authors  = { 'José Manuel Sánchez' => 'buscarini@gmail.com' }
	s.source   = { :git => 'https://buscarini@bitbucket.org/buscarini/jmsforms.git', :tag => "#{s.version}", :submodules => true }
	s.requires_arc = true

	s.ios.deployment_target = '7.0'

	s.source_files = 'JMSFormsExample/JMSForms/**/*.{h,m}' 
	s.resource_bundle = { 'JMSForms' => 'JMSFormsExample/JMSForms/**/*.{xib,lproj}' }
	
	s.dependency 'BMF/Core'
#	pod 'BMF', :git => 'git@bitbucket.org:buscarini/bmf.git'

	#s.dependency 'CrashlyticsLumberjack'
#	pod 'CrashlyticsLumberjack', :git => 'https://buscarini@bitbucket.org/buscarini/crashlyticslumberjack.git'		

end