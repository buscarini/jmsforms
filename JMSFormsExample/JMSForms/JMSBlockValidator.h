//
//  JMSBlockValidator.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JMSValidatorProtocol.h"

typedef BOOL(^JMSFormValidationBlock)(id value);

@interface JMSBlockValidator : NSObject <JMSValidatorProtocol>

@property (nonatomic, copy) JMSFormValidationBlock validationBlock;

- (id)initWithBlock:(JMSFormValidationBlock) block;

@end
