//
//  JMSBlockValidator.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSBlockValidator.h"

@implementation JMSBlockValidator

- (id)initWithBlock:(JMSFormValidationBlock) block
{
    self = [super init];
    if (self) {
        self.validationBlock = block;
    }
    return self;
}

- (id)init {
	[NSException raise:@"Invalid initializer. Use initWithBlock instead" format:@""];
	return nil;
}

- (BOOL) validate:(id)value {
	if (!self.validationBlock) {
		[NSException raise:@"Block validator should have a block" format:@"%@",self];
	}
	return self.validationBlock(value);
}

@end
