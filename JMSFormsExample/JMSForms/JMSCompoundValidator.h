//
//  JMSCompoundValidator.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JMSValidatorProtocol.h"

@interface JMSCompoundValidator : NSObject <JMSValidatorProtocol>

@property (nonatomic, strong) NSArray *validators;

@end
