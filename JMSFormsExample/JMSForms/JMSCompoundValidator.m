//
//  JMSCompoundValidator.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSCompoundValidator.h"

@implementation JMSCompoundValidator

- (BOOL) validate:(id)value {
	for (id<JMSValidatorProtocol> validator in self.validators) {
		if (![validator validate:value]) return NO;
	}
	
	return YES;
}

@end
