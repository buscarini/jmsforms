//
//  JMSDatePickerEditField.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSDatePickerEditField.h"

#import <BMF/BMFAutoLayoutUtils.h>
#import <BMF/BMFAnimationUtils.h>

@implementation JMSDatePickerEditField

- (void) performSetupCell:(UITableViewCell *) cell {
		
	[cell.contentView addSubview:self.datePicker];

	[self.datePicker setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
	[self.datePicker setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
	
	[self.datePicker setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
	[self.datePicker setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
	
	[BMFAutoLayoutUtils fill:self.datePicker parent:cell.contentView margin:0];
}

- (void) focusFormField: (id) sender {
}

- (void) removeFocusFormField: (id) sender {
}

- (CGFloat) heightWithTableRowHeight:(CGFloat) tableRowHeight {
	return self.datePicker.intrinsicContentSize.height;
}

- (void) update { }

@end
