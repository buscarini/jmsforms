//
//  JMSFormDatePickerField.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormLabelField.h"

@interface JMSFormDatePickerField : JMSFormLabelField

@property (nonatomic, strong) UIDatePicker *datePicker;

@property (nonatomic, assign) UIDatePickerMode datePickerMode;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end
