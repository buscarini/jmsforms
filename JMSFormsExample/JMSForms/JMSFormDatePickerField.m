//
//  JMSFormDatePickerField.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormDatePickerField.h"

#import <BMF/BMFAutoLayoutUtils.h>
#import <BMF/BMFAnimationUtils.h>

#import "JMSFormProperty.h"

#import "JMSDatePickerEditField.h"

@interface JMSFormDatePickerField()

@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) JMSDatePickerEditField *editField;

@end

@implementation JMSFormDatePickerField

- (void) performSetupCell:(UITableViewCell *) cell {
	[super performSetupCell:cell];
	
	self.dateLabel = [UILabel new];
	self.dateLabel.text = [self displayedValue];
	
	[self.dateLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
	
	[cell.contentView addSubview:self.dateLabel];
	
	[cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[textLabel]-[dateLabel]-|" options:0 metrics:nil views:@{ @"textLabel" : self.labelField,@"dateLabel" : self.dateLabel }]];
	[BMFAutoLayoutUtils centerVertically:@[ self.dateLabel ] inParent:cell.contentView margin:0];
}

- (void) focusFormField:(id)sender {
	[self.formView endEditing:NO];
	
	if (self.readOnly) return;

	//	[self.switchField becomeFirstResponder];
	if (self.editField) {
		self.value = self.datePicker.date;
		if (self.saveAutomatically) [self save];
		
		self.editField = nil;
	}
	else {
		self.editField = [[JMSDatePickerEditField alloc] initInFormView:self.formView property:nil parameters:nil];
		self.editField.datePicker = self.datePicker;
	}
	
	self.formView.editField = self.editField;
}

- (void) removeFocusFormField:(id)sender {
	//	[self.switchField resignFirstResponder];
	self.value = self.datePicker.date;
	self.editField = nil;
	self.formView.editField = self.editField;
}

- (NSString *) displayedValue {
	if (![self.value isKindOfClass:[NSDate class]]) return self.value;
	return [self.dateFormatter stringFromDate:self.value];
}

#pragma mark Accessors

- (void) update {
	if (self.readOnly) [self removeFocusFormField:self];
}

- (UIDatePicker *) datePicker {
	if (!_datePicker) {
		self.datePicker = [[UIDatePicker alloc] init];
		if (self.value && [self.value isKindOfClass:[NSDate class]]) self.datePicker.date = self.value;
		self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
		
		self.datePicker.datePickerMode = self.datePickerMode;
		
		[self.datePicker addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	}

	return _datePicker;
}

- (NSDateFormatter *) dateFormatter {
	
	if (!_dateFormatter) {
		
		UIDatePickerMode mode = UIDatePickerModeDateAndTime;
		if (self.datePicker) mode = self.datePicker.datePickerMode;
		
		_dateFormatter = [NSDateFormatter new];
		if (mode==UIDatePickerModeDate || mode==UIDatePickerModeDateAndTime) {
			_dateFormatter.dateStyle = NSDateFormatterMediumStyle;
		}
		else {
			_dateFormatter.dateStyle = NSDateFormatterNoStyle;
		}
		
		if (mode==UIDatePickerModeTime || mode==UIDatePickerModeDateAndTime) {
			_dateFormatter.timeStyle = NSDateFormatterShortStyle;
		}
		else {
			_dateFormatter.timeStyle = NSDateFormatterNoStyle;
		}
	}
	
	return _dateFormatter;
}

-(void) setValue:(id)value {
	if (value && ![value isKindOfClass:[NSDate class]]) {
		[NSException raise:@"Invalid value class" format:@"It should be a date"];
	}
	[super setValue:value];
	
	if (value) {
		self.datePicker.date = value;
		self.dateLabel.text = [self.dateFormatter stringFromDate:self.datePicker.date];
	}
}

- (void) valueChanged:(id) sender {
	self.value = self.datePicker.date;
	if (self.saveAutomatically) [self save];
}

@end
