//
//  JMSFormField.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JMSFormViewProtocol.h"

@class JMSFormProperty;

@interface JMSFormField : NSObject

@property (nonatomic, strong) id value;
@property (nonatomic, weak) UIView<JMSFormViewProtocol> *formView;
@property (nonatomic, strong) JMSFormProperty *property;
@property (nonatomic, strong) NSDictionary *parameters;

@property (nonatomic, strong) UIColor *validationFailedColor;

@property (nonatomic, assign) BOOL readOnly;
@property (nonatomic, assign) BOOL saveAutomatically;

@property (nonatomic, assign) UIKeyboardType keyboardType;

@property (nonatomic, strong) UIImage *icon; // Icon to show to the left of the label
@property (nonatomic, strong) id emptyValue; // Will show this if there is no value

@property (nonatomic, weak) UITableViewCell *cell;

- (void) update;

- (id) initInFormView:(UIView<JMSFormViewProtocol> *) formView property:(JMSFormProperty *) property parameters:(NSDictionary *) parameters;

- (void) resetCell:(UITableViewCell *) cell;
- (void) setupCell:(UITableViewCell *) cell;
- (void) performSetupCell:(UITableViewCell *) cell;

- (void) layoutInCell:(UITableViewCell *) cell;

- (CGFloat) heightWithTableRowHeight:(CGFloat) tableRowHeight;

- (void) focusFormField: (id) sender;
- (void) removeFocusFormField: (id) sender;

- (BOOL) validate;
- (BOOL) save;

@end
