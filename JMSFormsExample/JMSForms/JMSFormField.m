//
//  JMSFormField.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormField.h"

#import "JMSFormProperty.h"

#import <BMF/BMF.h>
#import <BMF/BMFAnimationUtils.h>

@interface JMSFormField()

@end

@implementation JMSFormField

- (id) initInFormView:(UIView<JMSFormViewProtocol> *) formView property:(JMSFormProperty *) property parameters:(NSDictionary *) parameters {
    self = [super init];
    if (self) {
        self.formView = formView;
		self.property = property;
		self.parameters = parameters;
		[self performInit];
    }
    return self;
}

- (instancetype)init {
	[NSException raise:@"Wrong initializer" format:@"Use initInFormView instead"];
    return nil;
}

- (void) performInit {
	
}

#pragma mark Accessors

- (void) setReadOnly:(BOOL)readOnly {
	_readOnly = readOnly;
	[self update];
}

- (void) setKeyboardType:(UIKeyboardType)keyboardType {
	_keyboardType = keyboardType;
	[self update];
}

- (void) update {
	[NSException raise:@"Not implemented" format:@"Subclasses must implement update"];
}

- (id) value {
	if (!_value && self.emptyValue) return self.emptyValue;
	return _value;
}

#pragma mark Cell

- (void) resetCell:(UITableViewCell *) cell {
	[cell.contentView BMF_removeAllSubviews];
}

- (void) setupCell:(UITableViewCell *)cell {
	self.cell = cell;
	[self resetCell:cell];
	[self performSetupCell:cell];
	[self layoutInCell:cell];
	[self update];
}

- (void) performSetupCell:(UITableViewCell *) cell {
	[NSException raise:@"Not implemented" format:@"Subclasses must implement setupCell"];
}

- (void) focusFormField: (id) sender {
	[NSException raise:@"Not implemented" format:@"Subclasses must implement focusFormField"];
}

- (void) removeFocusFormField: (id) sender {
	[NSException raise:@"Not implemented" format:@"Subclasses must implement removeFocusFormField"];
}

- (void) layoutInCell:(UITableViewCell *) cell {}

- (CGFloat) heightWithTableRowHeight:(CGFloat) tableRowHeight {
	return tableRowHeight;
}

#pragma mark Validation

- (BOOL) validate {
	return [self.property validate:self.value];
}

- (void) showValidationFailedInView:(UIView *) view {
	view.backgroundColor = self.validationFailedColor;
	[UIView animateWithDuration:1 animations:^{
		view.backgroundColor = [UIColor clearColor];
	}];
}

- (void) validationFailed {
	[self showValidationFailedInView:self.cell.contentView];
}

#pragma mark Save data

- (BOOL) save {
	
	if (self.readOnly) {
		DDLogError(@"Tried to save a readonly field: %@",self);
		return NO;
	}
	
	if (![self validate]) {
		[self validationFailed];
		return NO;
	}
	
	[self.formView.entity setValue:self.value forKeyPath:self.property.entityPropertyName];
	
	return YES;
}

@end
