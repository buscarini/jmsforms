//
//  JMSFormLabelField.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormField.h"

@interface JMSFormLabelField : JMSFormField

@property (nonatomic, strong) NSString *labelText;
@property (nonatomic, strong) UILabel *labelField;
@property (nonatomic, strong) UIImageView *iconView;
@end
