//
//  JMSFormLabelField.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormLabelField.h"

#import <BMF/BMFAutoLayoutUtils.h>

@implementation JMSFormLabelField

- (void) performSetupCell:(UITableViewCell *)cell {
	self.labelField = [UILabel new];
	self.labelField.text = self.labelText;
	self.labelField.font = [UIFont boldSystemFontOfSize:16];
	self.labelField.numberOfLines = 0;
	self.labelField.lineBreakMode = NSLineBreakByWordWrapping;
	[cell.contentView addSubview:self.labelField];
	
	if (self.icon) {
		self.iconView = [UIImageView new];
		self.iconView.contentMode = UIViewContentModeCenter;
		self.iconView.image = self.icon;
		[cell.contentView addSubview:self.iconView];
	}
}

- (void) layoutInCell:(UITableViewCell *) cell {
	[super layoutInCell:cell];
	
	[self.labelField setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
	[self.labelField setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
		
	if (self.iconView) {
		[BMFAutoLayoutUtils sizeEqualContent:self.iconView];
		[BMFAutoLayoutUtils fillVertically:@[ self.iconView, self.labelField ] parent:cell.contentView margin:0];
		[BMFAutoLayoutUtils equalAttributes:@[ cell.contentView, self.iconView ] parent:cell.contentView attribute:NSLayoutAttributeLeft margin:-10 priority:UILayoutPriorityRequired];

//		[BMFAutoLayoutUtils equalAttributes:@[ cell, self.iconView ] parent:cell attribute:NSLayoutAttributeLeft margin:0 priority:UILayoutPriorityRequired];
		
		NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self.iconView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.labelField attribute:NSLayoutAttributeLeft multiplier:1 constant:-10];
		constraint.priority = 999;
		[cell.contentView addConstraint:constraint];
	}
	else {
		[BMFAutoLayoutUtils fillVertically:@[ self.labelField ] parent:cell margin:0];
		NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:cell.contentView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.labelField attribute:NSLayoutAttributeLeft multiplier:1 constant:-10];
		constraint.priority = 999;
		[cell.contentView addConstraint:constraint];
	}
	
	cell.contentView.translatesAutoresizingMaskIntoConstraints = YES;
}

@end
