//
//  JMSFormPickerField.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormLabelField.h"

@interface JMSFormPickerField : JMSFormLabelField

@property (nonatomic, strong) UIPickerView *pickerView;

@property (nonatomic, strong) NSArray *values;

@end
