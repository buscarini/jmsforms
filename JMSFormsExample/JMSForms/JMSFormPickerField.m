//
//  JMSFormPickerField.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormPickerField.h"

#import <BMF/BMFAutoLayoutUtils.h>
#import <BMF/BMFAnimationUtils.h>

#import "JMSFormProperty.h"
#import "JMSPickerEditField.h"

@interface JMSFormPickerField() <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UILabel *valueLabel;
@property (nonatomic, strong) JMSPickerEditField *editField;

@end

@implementation JMSFormPickerField

- (void) performSetupCell:(UITableViewCell *) cell {
	[super performSetupCell:cell];
	
	self.pickerView = [[UIPickerView alloc] init];
	self.pickerView.dataSource = self;
	self.pickerView.delegate = self;
	
	[self.pickerView setContentCompressionResistancePriority:UILayoutPriorityRequired-1 forAxis:UILayoutConstraintAxisHorizontal];

	if (!self.values) {
		[NSException raise:@"values array is required for using a picker" format:@"%@",self.values];
		return;
	}

	self.valueLabel = [UILabel new];
	self.valueLabel.text = self.value;
	
	[cell.contentView addSubview:self.valueLabel];
	
	[cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[textLabel]-[valueLabel]-|" options:0 metrics:nil views:@{ @"textLabel" : self.labelField,@"valueLabel" : self.valueLabel }]];
	[BMFAutoLayoutUtils centerVertically:@[ self.valueLabel ] inParent:cell.contentView margin:0];
}

- (void) focusFormField:(id)sender {
	[self.formView endEditing:NO];
	
	if (self.readOnly) return;

	//	[self.switchField becomeFirstResponder];
	if (self.editField) {
		self.value = self.values[[self.pickerView selectedRowInComponent:0]];
		self.editField = nil;
	}
	else {
		self.editField = [[JMSPickerEditField alloc] initInFormView:self.formView property:nil parameters:nil];
		self.editField.pickerView = self.pickerView;
	}
	
	self.formView.editField = self.editField;
}

- (void) removeFocusFormField:(id)sender {
	//	[self.switchField resignFirstResponder];
	self.editField = nil;
	self.formView.editField = self.editField;
}

- (NSString *) displayedValue {
	return self.value;
}

#pragma mark Accessors

- (void) update {
	if (self.readOnly) [self removeFocusFormField:self];
}

//- (id) value {
//	return self.values[[self.pickerView selectedRowInComponent:0]];
//}

-(void) setValue:(id)value {
	if (value && ![value isKindOfClass:[NSString class]]) {
		[NSException raise:@"Invalid value class" format:@"It should be a string"];
	}
	[super setValue:value];
	
	NSUInteger index = [self.values indexOfObject:value];
	if (index==NSNotFound) index = 0;
	[self.pickerView selectRow:index inComponent:0 animated:YES];
	self.valueLabel.text = value;
}

#pragma mark UIPickerViewDataSource

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return self.values.count;
}

- (NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	return self.values[row];
}

#pragma mark UIPickerViewDelegate

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	self.value = self.values[row];
	if (self.saveAutomatically) [self save];
}

@end
