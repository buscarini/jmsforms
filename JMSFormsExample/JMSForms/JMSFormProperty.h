//
//  JMSFormProperty.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JMSFormViewProtocol.h"
#import "JMSValidatorProtocol.h"

typedef enum : NSUInteger {
    JMSFormPropertyString,
    JMSFormPropertyPhone,
    JMSFormPropertyText,
    JMSFormPropertyPassword,
	JMSFormPropertyEmail,
	JMSFormPropertyUrl,
	JMSFormPropertyOption,
	JMSFormPropertyDate,
	JMSFormPropertyTime,
	JMSFormPropertyDateTime,
	JMSFormPropertyBirthday,
	JMSFormPropertyBool
} JMSFormPropertyType;

typedef enum : NSUInteger {
    JMSFormPropertyTextSizeSmall, // ~3 lines of text
    JMSFormPropertyTextSizeMedium, // ~6 lines of text
    JMSFormPropertyTextSizeBig, // ~10 lines of text
} JMSFormPropertyTextSize;

@class JMSFormField;

typedef BOOL(^JMSPropertyValidationBlock)(id value);

@interface JMSFormProperty : NSObject <JMSValidatorProtocol>

@property (nonatomic, strong) NSString *labelText;
@property (nonatomic, strong) NSString *entityPropertyName;
@property (nonatomic, assign) JMSFormPropertyType type;
@property (nonatomic, assign) JMSFormPropertyTextSize textSize;

@property (nonatomic, strong) UIImage *icon; // Icon to show to the left of the label
@property (nonatomic, strong) id emptyValue; // Will show this if there is no value

// For a JMSFormPropertyOption
@property (nonatomic, strong) NSArray *options;

@property (nonatomic, strong) NSDictionary *parameters;

// Validation
@property (nonatomic, assign) BOOL required;
@property (nonatomic, strong) id<JMSValidatorProtocol> validator;

+ (JMSFormProperty *) requiredPropertyWith:(NSString *)labelText entityPropertyName:(NSString *) entityPropertyName type:(JMSFormPropertyType) type;
+ (JMSFormProperty *) propertyWith:(NSString *)labelText entityPropertyName:(NSString *) entityPropertyName type:(JMSFormPropertyType) type;

- (JMSFormField *) formField:(NSObject *) entity inFormView:(UIView<JMSFormViewProtocol> *) formView;

@end
