//
//  JMSFormProperty.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormProperty.h"

#import "JMSFormTextField.h"
#import "JMSFormTextView.h"
#import "JMSFormSwitchField.h"
#import "JMSFormDatePickerField.h"
#import "JMSFormPickerField.h"

#import "JMSNotEmptyValidator.h"

#import <BMF/BMF.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation JMSFormProperty

+ (JMSFormProperty *) requiredPropertyWith:(NSString *)labelText entityPropertyName:(NSString *) entityPropertyName type:(JMSFormPropertyType) type {
	JMSFormProperty *property = [self propertyWith:labelText entityPropertyName:entityPropertyName type:type];
	property.required = YES;
	return property;
}

+ (JMSFormProperty *) propertyWith:(NSString *)labelText entityPropertyName:(NSString *) entityPropertyName type:(JMSFormPropertyType) type {
	
	JMSFormProperty *property = [JMSFormProperty new];
	
	property.labelText = [NSLocalizedString(labelText,nil) stringByAppendingString:@":"];
	property.entityPropertyName = entityPropertyName;
	property.type = type;
	
	return property;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.textSize = JMSFormPropertyTextSizeMedium;
    }
    return self;
}

- (JMSFormField *) formField:(NSObject *) entity inFormView:(UIView<JMSFormViewProtocol> *) formView {
	
	JMSFormField *field = nil;
	
	if (self.type==JMSFormPropertyString) {
		field = [[JMSFormTextField alloc] initInFormView:formView property:self parameters:self.parameters];
	}
	else if (self.type==JMSFormPropertyPassword) {
		field = [[JMSFormTextField alloc] initInFormView:formView property:self parameters:self.parameters];
		JMSFormTextField *textField = [JMSFormTextField BMF_cast:field];
		textField.secureTextEntry = YES;
	}
	else if (self.type==JMSFormPropertyPhone) {
		field = [[JMSFormTextField alloc] initInFormView:formView property:self parameters:self.parameters];
		JMSFormTextField *textField = [JMSFormTextField BMF_cast:field];
		textField.keyboardType = UIKeyboardTypePhonePad;
	}
	else if (self.type==JMSFormPropertyEmail) {
		field = [[JMSFormTextField alloc] initInFormView:formView property:self parameters:self.parameters];
		JMSFormTextField *textField = [JMSFormTextField BMF_cast:field];
		textField.keyboardType = UIKeyboardTypeEmailAddress;
	}
	else if (self.type==JMSFormPropertyUrl) {
		field = [[JMSFormTextField alloc] initInFormView:formView property:self parameters:self.parameters];
		JMSFormTextField *textField = [JMSFormTextField BMF_cast:field];
		textField.keyboardType = UIKeyboardTypeURL;
	}
	else if (self.type==JMSFormPropertyText) {
		field = [[JMSFormTextView alloc] initInFormView:formView property:self parameters:self.parameters];
		JMSFormTextView *textView = [JMSFormTextView BMF_cast:field];
		textView.textViewSize = self.textSize;
	}
	else if (self.type==JMSFormPropertyBool) {
		field = [[JMSFormSwitchField alloc] initInFormView:formView property:self parameters:self.parameters];
	}
	else if (self.type==JMSFormPropertyDate) {
		field = [[JMSFormDatePickerField alloc] initInFormView:formView property:self parameters:self.parameters];
		JMSFormDatePickerField *dateField = [JMSFormDatePickerField BMF_cast:field];
		dateField.datePickerMode = UIDatePickerModeDate;
	}
	else if (self.type==JMSFormPropertyTime) {
		field = [[JMSFormDatePickerField alloc] initInFormView:formView property:self parameters:self.parameters];
		JMSFormDatePickerField *dateField = [JMSFormDatePickerField BMF_cast:field];
		dateField.datePickerMode = UIDatePickerModeTime;
	}
	else if (self.type==JMSFormPropertyDateTime) {
		field = [[JMSFormDatePickerField alloc] initInFormView:formView property:self parameters:self.parameters];
		JMSFormDatePickerField *dateField = [JMSFormDatePickerField BMF_cast:field];
		dateField.datePickerMode = UIDatePickerModeDateAndTime;
	}
	else if (self.type==JMSFormPropertyBirthday) {
		field = [[JMSFormDatePickerField alloc] initInFormView:formView property:self parameters:self.parameters];
		JMSFormDatePickerField *dateField = [JMSFormDatePickerField BMF_cast:field];
		dateField.datePickerMode = UIDatePickerModeDate;
		dateField.datePicker.maximumDate = [NSDate date];
	}
	else if (self.type==JMSFormPropertyOption) {
		field = [[JMSFormPickerField alloc] initInFormView:formView property:self parameters:self.parameters];
		JMSFormPickerField *optionField = [JMSFormPickerField BMF_cast:field];
		
		NSMutableArray *values = [NSMutableArray array];
		for (NSString *option in self.options) {
			[values addObject:NSLocalizedString(option, nil)];
		}
		optionField.values = values;
	}
	
	JMSFormLabelField *labelField = [JMSFormLabelField BMF_cast:field];
	labelField.labelText = self.labelText;
	
	RAC(field,value) = [entity rac_valuesForKeyPath:self.entityPropertyName observer:self];
	
	field.emptyValue = self.emptyValue;
	field.icon = self.icon;
	
	if (!field) {
		DDLogError(@"Couldn't create field: %@",[self typeString]);
	}
	
	return field;
}

- (NSString *) typeString {
	NSDictionary *typeDic = @{
							  @(JMSFormPropertyString) : @"String",
							  @(JMSFormPropertyText) : @"Text",
							  @(JMSFormPropertyPassword) : @"Password",
							  @(JMSFormPropertyEmail) : @"Email",
							  @(JMSFormPropertyDate) : @"Date",
							  @(JMSFormPropertyTime) : @"Time",
							  @(JMSFormPropertyDateTime) : @"Time",
							  @(JMSFormPropertyBool) : @"Bool"
							  };
	return typeDic[@(self.type)];
}

- (NSString *) description {
	return [NSString stringWithFormat:@"%p label: %@ property: %@ type: %@",self,self.labelText,self.entityPropertyName,[self typeString]];
}

#pragma mark JMSValidatorProtocol

- (BOOL) validate:(id)value {
	if (self.required) {
		JMSNotEmptyValidator *requiredValidator = [JMSNotEmptyValidator new];
		return [requiredValidator validate:value];
	}
	if (self.validator) return [self.validator validate:value];
	return YES;
}

@end
