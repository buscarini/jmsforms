//
//  JMSFormSwitchField.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormLabelField.h"

@interface JMSFormSwitchField : JMSFormLabelField

@property (nonatomic, strong) UISwitch *switchField;

@end
