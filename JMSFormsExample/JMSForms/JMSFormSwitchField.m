//
//  JMSFormSwitchField.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormSwitchField.h"

#import <BMF/BMFAutoLayoutUtils.h>
#import <BMF/BMFAnimationUtils.h>

#import "JMSFormProperty.h"

@interface JMSFormSwitchField()

@end

@implementation JMSFormSwitchField

- (void) performSetupCell:(UITableViewCell *) cell {
	[super performSetupCell:cell];
	
	self.switchField = [[UISwitch alloc] init];
	self.switchField.on = [self.value boolValue];
	[self.switchField addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	
	[cell.contentView addSubview:self.switchField];
	
	[self.switchField setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
//	[self.switchField setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
	[self.switchField setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];

	
	[cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[textLabel]-[switchField]-|" options:0 metrics:nil views:@{ @"textLabel" : self.labelField,@"switchField" : self.switchField }]];
	[BMFAutoLayoutUtils centerVertically:@[ self.switchField ] inParent:cell.contentView margin:0];
}

- (void) focusFormField:(id)sender {
	[self.formView endEditing:NO];
//	[self.switchField becomeFirstResponder];
}

- (void) removeFocusFormField:(id)sender {
	self.value = @(self.switchField.on);
//	[self.switchField resignFirstResponder];
}

#pragma mark Accessors

- (void) update {
	self.switchField.enabled = !self.readOnly;
}

-(void) setValue:(id)value {
	if (![value isKindOfClass:[NSNumber class]]) {
		[NSException raise:@"Invalid value class" format:@"It should be a number"];
	}
	[super setValue:value];
	self.switchField.on = [value boolValue];
	if (self.saveAutomatically) [self save];
}

- (void) valueChanged:(id) sender {
	self.value = @(self.switchField.on);
}


@end
