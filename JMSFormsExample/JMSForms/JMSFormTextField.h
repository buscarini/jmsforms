//
//  JMSFormTextField.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormLabelField.h"

@interface JMSFormTextField : JMSFormLabelField

@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, assign) BOOL secureTextEntry;
@property (nonatomic, assign) UITextAutocorrectionType autocorrectionType;

@end
