//
//  JMSFormTextField.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormTextField.h"

#import <BMF/BMFAutoLayoutUtils.h>
#import <BMF/BMFAnimationUtils.h>

#import "JMSFormProperty.h"

@interface JMSFormTextField() <UITextFieldDelegate>

@end

@implementation JMSFormTextField

- (void) performInit {
	self.autocorrectionType = UITextAutocorrectionTypeNo;
}

- (void) performSetupCell:(UITableViewCell *) cell {
	[super performSetupCell:cell];

	self.textField = [[UITextField alloc] init];
	self.textField.delegate = self;
	self.textField.text = self.value;
	[self.textField addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventEditingChanged];
	
	[cell.contentView addSubview:self.textField];
}

- (void) layoutInCell:(UITableViewCell *)cell {
	[super layoutInCell:cell];
	
	[self.textField setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
	
	[cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[textLabel]-(10@999)-[textField]|" options:0 metrics:nil views:@{ @"textLabel" : self.labelField,@"textField" : self.textField }]];
	[BMFAutoLayoutUtils fillVertically:@[ self.textField ] parent:cell.contentView margin:0];
}


- (void) focusFormField:(id)sender {
	[self.textField becomeFirstResponder];
}

- (void) removeFocusFormField:(id)sender {
	self.value = self.textField.text;
//	[self.textField resignFirstResponder];
}

#pragma mark Accessors

- (void) setSecureTextEntry:(BOOL)secureTextEntry {
	_secureTextEntry = secureTextEntry;
	[self update];
}

- (void) setAutocorrectionType:(UITextAutocorrectionType)autocorrectionType {
	_autocorrectionType = autocorrectionType;
	[self update];
}

- (void) update {
	self.textField.enabled = !self.readOnly;
	self.textField.keyboardType = self.keyboardType;
	self.textField.secureTextEntry = self.secureTextEntry;
	self.textField.autocorrectionType = self.autocorrectionType;
}

- (void) setValue:(id)value {
	[super setValue:value];
	self.textField.text = value;
}

- (void) valueChanged: (id) sender {
	self.value = self.textField.text;
	if (self.saveAutomatically) {
		[self save];
	}
}

#pragma mark UITextFieldDelegate

- (void) textFieldDidBeginEditing:(UITextField *)textField {
	[self.formView focusField:self];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
	self.value = self.textField.text;
	[self.formView nextField:self];
	return YES;
}

@end
