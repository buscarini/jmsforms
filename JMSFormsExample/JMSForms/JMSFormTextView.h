//
//  JMSFormTextView.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormLabelField.h"
#import "JMSFormProperty.h"

@interface JMSFormTextView : JMSFormLabelField

@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, assign) BOOL secureTextEntry;

@property (nonatomic, assign) JMSFormPropertyTextSize textViewSize;


@end
