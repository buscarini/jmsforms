//
//  JMSFormTextView.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormTextView.h"

#import <BMF/BMFAutoLayoutUtils.h>
#import <BMF/BMFAnimationUtils.h>

#import "JMSFormProperty.h"

@interface JMSFormTextView() <UITextViewDelegate>

@end

@implementation JMSFormTextView

- (void) performSetupCell:(UITableViewCell *) cell {
	[super performSetupCell:cell];
	
	self.textView = [[UITextView alloc] init];
	self.textView.delegate = self;
	self.textView.text = self.value;
	
	self.textView.backgroundColor = [UIColor clearColor];
	self.textView.opaque = NO;

	[cell.contentView addSubview:self.textView];
}

- (void) layoutInCell:(UITableViewCell *) cell {
	[self.labelField setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
	[self.labelField setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
	
	[self.labelField setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
	
	[self.textView setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
//	[self.textView setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];

	
	[cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(5)-[textLabel]-[textView]-(5)-|" options:0 metrics:nil views:@{ @"textLabel" : self.labelField,@"textView" : self.textView }]];
	[BMFAutoLayoutUtils fillHorizontally:@[ self.labelField, self.textView ] parent:cell.contentView margin:10];
}

- (void) focusFormField:(id)sender {
	[self.textView becomeFirstResponder];
}

- (void) removeFocusFormField:(id)sender {
	self.value = self.textView.text;
//	[self.textView resignFirstResponder];
}

#pragma mark Accessors

- (void) setSecureTextEntry:(BOOL)secureTextEntry {
	_secureTextEntry = secureTextEntry;
	[self update];
}

- (void) update {
	self.textView.editable = !self.readOnly;
	self.textView.keyboardType = self.keyboardType;
	self.textView.secureTextEntry = self.secureTextEntry;
}

-(void) setValue:(id)value {
	[super setValue:value];
	self.textView.text = value;
}

#pragma mark UITextViewDelegate

- (void) textViewDidBeginEditing:(UITextView *)textView {
	[self.formView focusField:self];
}

- (void) textViewDidChange:(UITextView *)textView {
	self.value = self.textView.text;
	if (self.saveAutomatically) [self save];
}

- (void) textViewDidEndEditing:(UITextView *)textView {
//	[self.formView nextField:self];
}

- (CGFloat) heightWithTableRowHeight:(CGFloat) tableRowHeight {
	
	CGFloat textViewHeight;
	if (self.textViewSize==JMSFormPropertyTextSizeSmall) {
		textViewHeight = 60;
	}
	else if (self.textViewSize==JMSFormPropertyTextSizeMedium) {
		textViewHeight = 90;
	}
	else {
		textViewHeight = 140;
	}
	
	return textViewHeight+20+5*3;
}

@end
