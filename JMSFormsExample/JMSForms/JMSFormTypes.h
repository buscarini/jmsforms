
//#define JMSLocalized(string,comment) NSLocalizedStringFromTableInBundle(string,@"JMSLocalizable",[NSBundle bundleWithIdentifier:@"JMSForms"],comment)

#define JMSLocalized(string,comment) NSLocalizedStringFromTableInBundle(string,@"JMSLocalizable",[NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"JMSForms" withExtension:@"bundle"]],comment)

// TODO Change this when I know how to generate a bundle and get the strings from there
//#define JMSLocalized(string,comment) NSLocalizedString(string,comment)
