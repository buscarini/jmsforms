//
//  JMSFormView.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JMSFormViewProtocol.h"

typedef void(^JMSValueChangedBlock)();

@interface JMSFormView : UIView <JMSFormViewProtocol>

/// Makes a form for the object entity passed. Properties should be an array of JMSFormProperty. There you can specify which properties to make fields for, the types and the order of these
- (BOOL) loadforEntity:(NSObject *) entity properties:(NSArray *) properties;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, assign) BOOL readOnly;
@property (nonatomic, strong) UIView *inputAccessoryView;
@property (nonatomic, strong) JMSFormField *focusedField;
@property (nonatomic, strong) NSObject *entity;

@property (nonatomic, strong) UIColor *validationFailedColor;

/// Extra field that can be used to modify the current focused field (for pickers, for example)
@property (nonatomic, strong) JMSFormField *editField;


@property (nonatomic, strong) UIBarButtonItem *previousButton;
@property (nonatomic, strong) UIBarButtonItem *nextButton;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@property (nonatomic, copy) JMSValueChangedBlock valueChangedBlock;

/// Saves automatically to the entity when the user changes a value
@property (nonatomic, assign) BOOL saveAutomatically;

@property (nonatomic, readonly) BOOL isValid;

@property (nonatomic, readonly) NSMutableArray *fields;

/// Returns true if the values pass validation
- (BOOL) save;

@end
