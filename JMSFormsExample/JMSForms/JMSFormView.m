//
//  JMSFormView.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormView.h"

#import "JMSFormProperty.h"
#import "JMSFormField.h"
#import "JMSFormLabelField.h"

#import "JMSFormTypes.h"

#import <BMF/BMF.h>
#import <BMF/BMFAutoLayoutUtils.h>

@interface JMSFormView() <UITableViewDataSource,UITableViewDelegate>
	@property (nonatomic, strong) NSArray *properties;
	@property (nonatomic, strong) NSMutableArray *fields;
@end

@implementation JMSFormView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self performInit];
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self performInit];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
		[self performInit];
    }
    return self;
}

- (void) performInit {
	self.saveAutomatically = NO;
	self.validationFailedColor = [UIColor colorWithRed:1.000 green:0.941 blue:0.500 alpha:0.490];

}

- (void) dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL) loadforEntity:(NSObject *) entity properties:(NSArray *) properties {
	
	if (!entity) {
		[NSException raise:@"Entity can't be nil" format:@""];
		return NO;
	}
	
	self.entity = entity;
	self.properties = properties;
	
	self.tableView = [[UITableView alloc] init];
	[self addSubview:self.tableView];
	[BMFAutoLayoutUtils fill:self.tableView parent:self margin:0];
	self.tableView.dataSource = self;
	self.tableView.delegate = self;
	[self.tableView BMF_hideSeparatorsForEmptyCells];
	
	[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
	
	self.fields = [NSMutableArray array];
	for (JMSFormProperty *property in properties) {
		JMSFormField *field = [property formField:entity inFormView:self];
		if (field)  {
			[self.fields addObject:field];
		}
		else {
			DDLogError(@"Can't create field for property: %@",property);
		}
	}
	
	[self updateFields];
	
	[self.tableView reloadData];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

	return YES;
}

#pragma mark Accessors

- (void) setFocusedField:(JMSFormField *)focusedField {
	_focusedField = focusedField;
	[self updateButtons];
	
#warning Do this right (The form field should inform the formview when a value has changed)
	if (self.valueChangedBlock) self.valueChangedBlock();
}

- (UIView *) inputAccessoryView {
	
#warning Change the uitoolbar for a UIInputView
	if (!_inputAccessoryView) {
		UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 44)];
		
		self.previousButton = [[UIBarButtonItem alloc] initWithTitle:JMSLocalized(@"Previous", nil) style:UIBarButtonItemStylePlain target:self action:@selector(previousField:)];
		self.nextButton = [[UIBarButtonItem alloc] initWithTitle:JMSLocalized(@"Next", nil) style:UIBarButtonItemStylePlain target:self action:@selector(nextField:)];
		self.doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
		
		toolbar.items = @[ self.previousButton, self.nextButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], self.doneButton ];
		
		_inputAccessoryView = toolbar;
		[self updateButtons];
	}
	
	return _inputAccessoryView;
}

- (void) setEditField:(JMSFormField *)editField {
	
	if (editField==_editField) return;
	
	JMSFormField *oldField = _editField;
	_editField = editField;
	
	@try {
		
		[self.tableView beginUpdates];
		
		NSInteger index = [self.fields indexOfObject:self.focusedField];
		
		if (oldField) {
			[self.tableView deleteRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:index+1 inSection:0] ] withRowAnimation:UITableViewRowAnimationFade];
		}

		if (_editField) {
			[self.tableView insertRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:index+1 inSection:0] ] withRowAnimation:UITableViewRowAnimationFade];
		}
		
		[self.tableView endUpdates];
	}
	@catch (NSException *exception) {
		DDLogError(@"Exception inserting edit field: %@",exception);
		[self.tableView reloadData];
	}
}

- (void) setReadOnly:(BOOL)readOnly {
	_readOnly = readOnly;
	
	self.tableView.allowsSelection = !_readOnly;
	
	[self updateFields];
}

- (void) setSaveAutomatically:(BOOL)saveAutomatically {
	_saveAutomatically = saveAutomatically;
	[self updateFields];
}

- (void) updateFields {
	for (JMSFormField *field in self.fields) {
		field.readOnly = self.readOnly;
		field.validationFailedColor = self.validationFailedColor;
		field.saveAutomatically = self.saveAutomatically;
	}
	
	if (!self.readOnly) self.editField = NO;
	
//	[self.tableView reloadData];
}

#pragma mark Actions

- (void) previousField:(id) sender {
	NSUInteger index = [self.fields indexOfObject:self.focusedField];
	if (index!=NSNotFound) {
		index--;
		[self focusField:self.fields[index]];
	}
	
	[self updateButtons];
}

- (void) nextField:(id) sender {
	NSUInteger focusedIndex = [self.fields indexOfObject:self.focusedField];
	NSUInteger index = focusedIndex;
	if (index!=NSNotFound) {
		index++;
		if (index<self.fields.count) {
			[self focusField:self.fields[index]];
		}
	}
	[self updateButtons];
}

- (void) done:(id) sender {
	[self.focusedField removeFocusFormField:sender];
	self.focusedField = nil;
	[self endEditing:YES];
}

- (void) updateButtons {
	self.previousButton.enabled = YES;
	self.nextButton.enabled = YES;
	
	NSUInteger index = [self.fields indexOfObject:self.focusedField];
	if (index==0 || index==NSNotFound) {
		self.previousButton.enabled = NO;
	}
	
	if (index==self.fields.count-1) {
		self.nextButton.enabled = NO;
	}
}

- (void) focusField:(JMSFormField *) field {
//	[self endEditing:YES];
	if (self.focusedField && self.focusedField!=field) [self.focusedField removeFocusFormField:self];
	self.focusedField = field;
	[_focusedField focusFormField:self];
	[self scrollToCurrent];
}

- (void) scrollToCurrent {
	@try {
		[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.fields indexOfObject:self.focusedField] inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	}
	@catch (NSException *exception) {
		DDLogError(@"Exception scrolling form table: %@",exception);
	}
}

#pragma mark UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSInteger numRows = self.fields.count;
	if (self.editField) numRows++;
	return numRows;
}

- (JMSFormField *) fieldAtIndex:(NSInteger) index {
	
	if (self.editField) {
		NSInteger currentFieldIndex = [self.fields indexOfObject:self.focusedField];
		if (index<=currentFieldIndex) {
			return self.fields[index];
		}
		else if (index==currentFieldIndex+1) {
			return self.editField;
		}
		else {
			return self.fields[index-1];
		}
	}
	else {
		return self.fields[index];
	}
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
	
	JMSFormField *field = [self fieldAtIndex:indexPath.row];
	[field setupCell:cell];
	
	return cell;
}

#pragma mark UITableViewDelegate

//- (CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//	JMSFormField *field = [self fieldAtIndex:indexPath.row];
//	return [field heightWithTableRowHeight:self.tableView.rowHeight];
//}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	/*if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
		NSMutableArray *labelFields = [NSMutableArray array];
		for (JMSFormField *field in fields) {
			JMSFormLabelField *labelField = [JMSFormLabelField BMF_cast:field];
			if (labelField && labelField.labelField) [labelFields addObject:labelField.labelField];
		}
		
		[BMFAutoLayoutUtils equalWidths:labelFields inParent:self.tableView priority:UILayoutPriorityDefaultHigh];
	}*/

}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	JMSFormField *field = [self fieldAtIndex:indexPath.row];
	return [field heightWithTableRowHeight:self.tableView.rowHeight];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	JMSFormField *field = [self fieldAtIndex:indexPath.row];
	[self focusField:field];
}

- (BOOL) isValid {
	for (JMSFormField *field in self.fields) {
		if (![field validate]) return NO;
	}
	
	return YES;
}

- (BOOL) save {
	
	BOOL result = YES;
	for (JMSFormField *field in self.fields) {
		if (![field save]) {
			result = NO;
		}
	}
	
	return result;
}

#pragma mark Keyboard notifications


- (void) keyboardWillShow: (NSNotification *) note {
	NSDictionary *info = [note userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
//    _animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];

    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardFrame.size.height+self.inputAccessoryView.bounds.size.height-(self.superview.bounds.size.height-self.bounds.size.height+self.frame.origin.y), 0);
	self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
	[self scrollToCurrent];
}

- (void) keyboardDidShow: (NSNotification *) note {
	[self scrollToCurrent];
}

- (void) keyboardWillHide: (NSNotification *) note {
//	NSDictionary *info = [note userInfo];
//    _animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	self.tableView.contentInset = UIEdgeInsetsZero;
	self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
	[self scrollToCurrent];
}


@end
