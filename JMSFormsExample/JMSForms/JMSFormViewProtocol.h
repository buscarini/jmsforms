//
//  JMSFormViewProtocol.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JMSFormField;

@protocol JMSFormViewProtocol <NSObject>

@property (nonatomic, strong) UIView *inputAccessoryView;
@property (nonatomic, strong) NSObject *entity;

/// Extra field that can be used to modify the current focused field (for pickers, for example)
@property (nonatomic, strong) JMSFormField *editField;

- (void) previousField:(id) sender;
- (void) nextField:(id) sender;
- (void) done:(id) sender;

- (void) focusField:(JMSFormField *) field;

@end
