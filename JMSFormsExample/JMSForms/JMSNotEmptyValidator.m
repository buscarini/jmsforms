//
//  JMSNotEmptyValidator.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSNotEmptyValidator.h"

#import <BMF/BMF.h>

@implementation JMSNotEmptyValidator

- (BOOL) validate:(id)value {
	if (!value) return NO;
	NSString *stringValue = [NSString BMF_cast:value];
	if (stringValue && stringValue.length==0) return NO;
	return YES;
}

@end
