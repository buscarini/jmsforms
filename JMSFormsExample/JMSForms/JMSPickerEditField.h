//
//  JMSPickerEditField.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormField.h"

@interface JMSPickerEditField : JMSFormField

@property (nonatomic, strong) UIPickerView *pickerView;

@end
