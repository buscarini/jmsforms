//
//  JMSPickerEditField.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSPickerEditField.h"

#import <BMF/BMFAutoLayoutUtils.h>
#import <BMF/BMFAnimationUtils.h>

@implementation JMSPickerEditField

- (void) performSetupCell:(UITableViewCell *) cell {
	
	[cell.contentView addSubview:self.pickerView];

	[self.pickerView setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
	[self.pickerView setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];

	[self.pickerView setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
	[self.pickerView setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
	
	[BMFAutoLayoutUtils fill:self.pickerView parent:cell.contentView margin:0];
}

- (void) focusFormField: (id) sender {
}

- (void) removeFocusFormField: (id) sender {
}

- (CGFloat) heightWithTableRowHeight:(CGFloat) tableRowHeight {
	return self.pickerView.bounds.size.height;
}

- (void) update { }

@end
