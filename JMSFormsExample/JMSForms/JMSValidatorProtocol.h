//
//  JMSValidatorProtocol.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JMSValidatorProtocol <NSObject>

- (BOOL) validate:(id) value;

@end
