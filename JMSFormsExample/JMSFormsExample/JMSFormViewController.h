//
//  JMSFormViewController.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JMSFormView;
@class JMSPerson;

@interface JMSFormViewController : UIViewController

@property (strong, nonatomic) JMSPerson *person;
@property (weak, nonatomic) IBOutlet JMSFormView *formView;

- (IBAction)save:(id)sender;

@end
