//
//  JMSFormViewController.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSFormViewController.h"

#import "JMSFormView.h"
#import "JMSPerson.h"
#import "JMSFormField.h"
#import "JMSFormProperty.h"

#import "JMSBlockValidator.h"

@interface JMSFormViewController ()

@end

@implementation JMSFormViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	if (!self.person) self.person = [JMSPerson sharedPerson];
	
	NSMutableArray *properties = [NSMutableArray array];

	[properties addObject:[JMSFormProperty requiredPropertyWith:@"Name" entityPropertyName:@"name" type:JMSFormPropertyString]];
	[properties addObject:[JMSFormProperty requiredPropertyWith:@"Surname" entityPropertyName:@"surname" type:JMSFormPropertyString]];
	[properties addObject:[JMSFormProperty propertyWith:@"Birthday" entityPropertyName:@"birthDay" type:JMSFormPropertyBirthday]];

	JMSFormProperty *dateProperty = [JMSFormProperty propertyWith:@"Date" entityPropertyName:@"date" type:JMSFormPropertyDate];
	dateProperty.icon = [UIImage imageNamed:@"twitter"];
	dateProperty.emptyValue = @"A simple date";
	[properties addObject:dateProperty];
	
	JMSFormProperty *langProperty = [JMSFormProperty propertyWith:@"Language" entityPropertyName:@"language" type:JMSFormPropertyOption];
	langProperty.options = @[ @"Spanish", @"English", @"German", @"French" ];
	[properties addObject:langProperty];


	JMSFormProperty *memberProperty = [JMSFormProperty propertyWith:@"Member of the League of Shadows" entityPropertyName:@"isMember" type:JMSFormPropertyBool];
	memberProperty.validator = [[JMSBlockValidator alloc] initWithBlock:^BOOL(id value) {
		return [value boolValue];
	}];
	[properties addObject:memberProperty];

	JMSFormProperty *descriptionProperty = [JMSFormProperty requiredPropertyWith:@"Description" entityPropertyName:@"text" type:JMSFormPropertyText];
	descriptionProperty.textSize = JMSFormPropertyTextSizeBig;
	[properties addObject:descriptionProperty];
	
	[properties addObject:[JMSFormProperty requiredPropertyWith:@"Name" entityPropertyName:@"name" type:JMSFormPropertyString]];
	[properties addObject:[JMSFormProperty requiredPropertyWith:@"Surname" entityPropertyName:@"surname" type:JMSFormPropertyString]];
	[properties addObject:[JMSFormProperty propertyWith:@"Birthday" entityPropertyName:@"birthDay" type:JMSFormPropertyBirthday]];

	
	[properties addObject:[JMSFormProperty requiredPropertyWith:@"Name" entityPropertyName:@"name" type:JMSFormPropertyString]];
	[properties addObject:[JMSFormProperty requiredPropertyWith:@"Surname" entityPropertyName:@"surname" type:JMSFormPropertyString]];
	[properties addObject:[JMSFormProperty propertyWith:@"Birthday" entityPropertyName:@"birthDay" type:JMSFormPropertyBirthday]];

//	[properties addObject:[JMSFormProperty propertyWith:@"Name" entityPropertyName:@"name" type:JMSFormPropertyString]];
//	[properties addObject:[JMSFormProperty propertyWith:@"Surname" entityPropertyName:@"surname" type:JMSFormPropertyString]];

	[self.formView loadforEntity:self.person properties:properties];
	
//	self.formView.readOnly = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)save:(id)sender {
	if ([self.formView save]) {
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

@end
