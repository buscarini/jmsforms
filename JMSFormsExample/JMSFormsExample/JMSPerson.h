//
//  JMSPerson.h
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JMSPerson : NSObject

@property (nonatomic, strong) NSString *ident;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *surname;
@property (nonatomic, strong) NSDate *birthDay;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, assign) BOOL isMember;
@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSString *text;

+ (JMSPerson *) sharedPerson;

@end
