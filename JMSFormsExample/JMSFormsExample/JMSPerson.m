//
//  JMSPerson.m
//  JMSFormsExample
//
//  Created by Jose Manuel Sánchez Peñarroja on 24/01/14.
//  Copyright (c) 2014 JMS. All rights reserved.
//

#import "JMSPerson.h"

static JMSPerson *instance = nil;

@implementation JMSPerson

+ (JMSPerson *) sharedPerson {
	if (!instance) instance = [JMSPerson new];
	return instance;
}

@end
